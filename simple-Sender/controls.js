// Set some initial variables
const buttonHandler = $("#playback");
const streamUrl = "https://fillyradio.com/stream-320k";
var smReady = false;
const sm = soundManager;
smObject = null;

// We're using soundmanager here, not for any particular reason,
// other than it's a simple API for sound playback.
// You can use pretty much whatever, SM2 is only here to demonstrate
// that we can swap over the playback controls (localPlayback) to
// the Chromecast controls (remotePlayback).

// Setup the soundManager object
soundManager.setup({
  preferFlash: false,
  useHTML5Audio: true,
  onready: function() {
    smReady = true;
  }
});

// Handles the button being presed.
var handlePlayback = function() {
  // Checks to see if we have a Chromecast connected. This also does
  // nothing if the Chromecast is
  if (
    castState === "NOT_CONNECTED" ||
    castState === "NO_DEVICES_AVAILABLE" ||
    !castState
  ) {
    localPlayback();
  } else if (castState === "CONNECTED") {
    remotePlayback();
  } else if (castState === "CONNECTING") {
  }
};

var createSound = function() {
  smObject = soundManager.createSound({
    id: "playback",
    url: streamUrl,
    autoLoad: true,
    autoPlay: false,
    volume: 50
  });
};

var localPlayback = function() {
  if (smReady) {
    if (sm.getSoundById("playback")) {
      sm.destroySound("playback");
      buttonHandler.html("Play");
    } else if (!sm.getSoundById("playback")) {
      createSound();
      smObject.play();
      buttonHandler.html("Stop");
    }
  }
};

var remotePlayback = function() {
  if (sm.getSoundById("playback")) {
    sm.destroySound("playback");
  }
  if (!playerState.value || !playerController) {
    remoteToLocalState("LOADING");
    startCast();
  } else {
    playerController.stop();
    playerState = false;
  }
};

var killLocalPlayback = function() {
  if (sm.getSoundById("playback")) {
    sm.destroySound("playback");
  }
};
