// Create empty metadata object for the database to fill
var metadata = {};

// Configure the Firebase settings
var config = {
  databaseURL: "https://fillydelphia-radio.firebaseio.com",
  projectId: "fillydelphia-radio"
};
// Init Firebase
firebase.initializeApp(config);

// Get the station data
var database = firebase.database();
var stationData = database.ref("rest/fillydelphia-radio/now-playing");

// Un-enscape HTML characters (escaped in LiquidSoap)
function htmlDecode(input) {
  var e = document.createElement("div");
  e.innerHTML = input;
  return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
}

// When the local DB state is updated, apply the data to the metadata object
stationData.on("value", function(s) {
  console.log(s.val().album);
  metadata.artist = s.val().artist ? htmlDecode(s.val().artist) : null;
  metadata.album = s.val().album ? htmlDecode(s.val().album) : null;
  metadata.track = s.val().title ? htmlDecode(s.val().title) : null;
  metadata.coverUrl =
    "https://fillyradio.com/cover?nocache=" + new Date().getTime();
});
